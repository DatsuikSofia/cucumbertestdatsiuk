@MarkMessage
Feature: Delete ImportantMessage

  Background:
    Given User navigate to Gmail  page on URL

  Scenario Outline: Mark and Delete 3 ImportantMessage
    When User fill "<email>" in email field and click next button
    Then I should see Forgot password message
    When User fill  "<password>" in password field and click next button
    Then User navigate to Gmail Home Page
    And User select 3 important message in inbox
    Then Conversations mark as important is visible
    And User click Important buton
    And User select 3 message
    When User click delete button
    Then "Conversation is deleted" message is visible
    Examples:
      | email                       |  | password      |
      | test3mailSelenium@gmail.com |  | DriverManager |

