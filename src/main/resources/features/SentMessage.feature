@SentMessage
Feature: Send message in gmail

  Background:
    Given User navigate to Gmail  page on URL

  Scenario Outline: Login and send message
    When User fill "<email>" in email field and click next button
    Then I should see Forgot password message
    When User fill  "<password>" in password field and click next button
    Then User navigate to Gmail Home Page
    And User click compose button
    And Message popup is visible
    And User enter "<recepient>"recepients "<subject>" in subject message "<message>"message in text box
    And User click sent button
    Examples:
      | email                       |  | password      |  | recepient         |  | subject |  | message  |
      | test3mailSelenium@gmail.com |  | DriverManager |  | sofdatc@gmail.com |  | hello   |  | Cucumber |
