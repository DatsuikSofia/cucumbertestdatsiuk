package stepDefinition;

import business.MarkAndDeleteImportantMessageBO;
import cucumber.api.java.en.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import static org.junit.Assert.*;

public class MarkConversation {
    private MarkAndDeleteImportantMessageBO markAndDeleteImportantMessageBO = new MarkAndDeleteImportantMessageBO();
    public static final Logger LOG = LogManager.getLogger(SendMessage.class);

    @And("^User select (\\d*) important message in inbox$")
    public void userSelectImportantMessageInInbox(int countMessage) throws Throwable {
        LOG.info("User mark" + countMessage + "message as important");
        markAndDeleteImportantMessageBO.markImportantMessages();
    }

    @And("^Conversations mark as important is visible$")
    public void conversationMarkAsImportant() {
        LOG.info("Conversations successfuly mark as important");
        assertTrue(markAndDeleteImportantMessageBO.messagesMarkedAsImportant());
    }

    @And("^User select (\\d+) message$")
    public void userSelectMessage(int choosenMessage) {
        LOG.info("Choose " + choosenMessage + "important message");
        markAndDeleteImportantMessageBO.chooseImportantMessages();
    }

    @And("^User click Important buton$")
    public void userClickImportantButon() {
        LOG.info("User navigate to important page");
        markAndDeleteImportantMessageBO.clickImportantButton();
    }

    @When("^User click delete button$")
    public void userClickDeleteButton() {
        LOG.info("Messages was deleted");
        markAndDeleteImportantMessageBO.deleteImportantMessages();
    }

    @Then("^\"([^\"]*)\" message is visible$")
    public void messageIsVisible() {
        LOG.info("Conversations was deleted");
        assertTrue(markAndDeleteImportantMessageBO.messagesDeleted());
    }
}
