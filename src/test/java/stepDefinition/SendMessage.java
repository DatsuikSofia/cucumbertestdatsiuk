package stepDefinition;

import business.AutorizationBO;
import business.WriteMessageBO;
import cucumber.api.PendingException;
import cucumber.api.java.en.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import static org.junit.Assert.*;

import util.DriverManager;


public class SendMessage {
    public static final Logger LOG = LogManager.getLogger(SendMessage.class);
    private AutorizationBO autorizationBO = new AutorizationBO();
    private WriteMessageBO writeMessageBO = new WriteMessageBO();

    @Then("^User navigate to Gmail Home Page$")
    public void userNavigateToGmailHomePage() {
        LOG.info("User enter correct data");
        assertTrue(autorizationBO.verifyLogInSuccessful());
    }

    @And("^User click compose button$")
    public void userClickComposeButton() {
        LOG.info("User click compose button");
        writeMessageBO.clickComposeButton();
    }

    @When("^Message popup is visible$")
    public void messagePopupIsVisible() {
        LOG.info("New message window is displayed");
        assertTrue(writeMessageBO.messagePopUpIsVisible());
    }


    @When("^User click sent button$")
    public void userClickSentButton() {
        LOG.info("User click sent button");
        writeMessageBO.sentNewMessage();
    }

    @Then("^Message sent successful is visible$")
    public void messageSentSuccessfulIsVisible() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }


    @Then("^I should see Forgot password message$")
    public void iShouldSeeForgotPasswordMessage() {
        LOG.info("User enter correct email");
        assertTrue(autorizationBO.verifyUserEnterCorrectEmail());
    }

    @Then("^User fill  \"([^\"]*)\" in password field and click next button$")
    public void userFillInPasswordFieldAndClickNextButton(String password) {
        LOG.info("User enter password" + password + "in password text box");
        autorizationBO.userEnterPasswordAndClickNextButton(password);
    }

    @Given("^User navigate to Gmail  page on URL$")
    public void userNavigateToGmailPageOnURL() {
        LOG.info("User navigate to gmail home page" + DriverManager.getWebDriver().getTitle());
        assertTrue("Gmail", DriverManager.getWebDriver().getTitle().contains("Gmail"));
    }

    @And("^User enter \"([^\"]*)\"recepients \"([^\"]*)\" in subject message \"([^\"]*)\"message in text box$")
    public void userEnterRecepientsInSubjectMessageMessageInTextBox(String res, String sub, String mess) throws Throwable {
        LOG.info("User sent message to " + res + "with subject" + sub + "with text" + mess);
        writeMessageBO.writeGmailMessage(res, sub, mess);
    }

    @When("^User fill \"([^\"]*)\" in email field and click next button$")
    public void userFillInEmailFieldAndClickNextButton(String email) throws Throwable {
        LOG.info("This step enter the email " + email + "on the login page.");
        autorizationBO.userEnterEmailAndClickNextButtton(email);
    }
}
