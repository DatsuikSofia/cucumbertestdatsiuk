package stepDefinition;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.log4j.Logger;
import util.DriverManager;
public class ServiceHooks {
    private static Logger LOG = Logger.getLogger(ServiceHooks.class.getName());

    @Before
    public void initializeTest(){
        DriverManager.getWebDriver().get("https://www.gmail.com");
    }

    @After
    public void driverTearDown(Scenario scenario){
        DriverManager.closeDriver();
        LOG.info("Browser closed");
    }
}